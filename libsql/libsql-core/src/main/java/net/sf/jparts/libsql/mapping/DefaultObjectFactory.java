package net.sf.jparts.libsql.mapping;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public class DefaultObjectFactory implements ObjectFactory {

    @Override
    public <T> T create(Class<T> clazz) {
        checkClass(clazz);
        try {
            return clazz.newInstance();
        } catch (InstantiationException | IllegalAccessException ex) {
            throw new ObjectCreationException("Can't create object of class " + clazz.getName() +
                    ", message = " + ex.getMessage(), ex);
        }
    }

    @Override
    public <T> T create(Class<T> clazz, Class<?>[] types, Object[] values) {
        checkClass(clazz);
        if (types == null || types.length < 1 || values == null || values.length < 1) {
            return create(clazz);
        }

        try {
            Constructor<T> c = clazz.getDeclaredConstructor(types);
            c.setAccessible(true);
            return c.newInstance(values);
        } catch (NoSuchMethodException ex) {
            StringBuilder sb = new StringBuilder("Can't find construtor ");
            sb.append(clazz.getName());
            sb.append("(");
            for (int i = 0; i < types.length; i++) {
                if (i > 0) {
                    sb.append(",");
                }
                sb.append(types[i].getName());
            }
            sb.append("), message = ");
            sb.append(ex.getMessage());

            throw new ObjectCreationException(sb.toString(), ex);
        } catch (InvocationTargetException | InstantiationException | IllegalAccessException ex) {
            throw new ObjectCreationException("Can't create new instance: " + ex.getMessage(), ex);
        }
    }

    private void checkClass(Class<?> clazz) {
        if (clazz == null) {
            throw new IllegalArgumentException("clazz must not be null");
        }
    }
}