package net.sf.jparts.libsql.mapping.types;

import org.joda.time.DateTime;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

public class DateTimeTypeHandler implements TypeHandler<DateTime> {

    @Override
    public DateTime getResult(ResultSet rs, String column) throws SQLException {
        Timestamp time = rs.getTimestamp(column);
        return (time == null) ? null : new DateTime(time);
    }
}
