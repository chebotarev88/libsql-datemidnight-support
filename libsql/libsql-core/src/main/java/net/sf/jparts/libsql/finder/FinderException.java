package net.sf.jparts.libsql.finder;

/**
 * May be thrown from {@link Finder}, {@link FinderResult} or {@link net.sf.jparts.libsql.LibSql} when a problem occurs,
 * note that <b>this exception (and it's subclasses) is a BETA version of API.</b>
 *
 * @since 2.0.0
 */
public class FinderException extends RuntimeException {

    public FinderException() {
    }

    public FinderException(String message) {
        super(message);
    }

    public FinderException(String message, Throwable cause) {
        super(message, cause);
    }

    public FinderException(Throwable cause) {
        super(cause);
    }

    public FinderException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
