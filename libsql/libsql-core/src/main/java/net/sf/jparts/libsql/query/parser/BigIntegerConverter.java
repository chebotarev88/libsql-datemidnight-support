package net.sf.jparts.libsql.query.parser;

import java.math.BigInteger;

public final class BigIntegerConverter extends AbstractIntegerConverter<BigInteger> {

    public BigIntegerConverter() {
    }

    public BigIntegerConverter(boolean safe, BigInteger errorValue, BigInteger nullValue, int radix) {
        super(safe, errorValue, nullValue, radix);
    }

    @Override
    protected BigInteger tryCreate(String value, int radix) throws NumberFormatException {
        return new BigInteger(value, radix);
    }
}
