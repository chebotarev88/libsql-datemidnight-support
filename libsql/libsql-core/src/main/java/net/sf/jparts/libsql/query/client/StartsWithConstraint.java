package net.sf.jparts.libsql.query.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "startsWith")
@XmlAccessorType(XmlAccessType.FIELD)
public class StartsWithConstraint implements Constraint {

    private static final long serialVersionUID = -2125918032581284873L;

    @XmlElement(required = true)
    private String property;

    @XmlElement(required = true)
    private String value;

    public StartsWithConstraint() {
    }

    public StartsWithConstraint(String property, String value) {
        this.property = property;
        this.value = value;
    }

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        StartsWithConstraint that = (StartsWithConstraint) o;

        if (property != null ? !property.equals(that.property) : that.property != null) return false;
        //noinspection RedundantIfStatement
        if (value != null ? !value.equals(that.value) : that.value != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = property != null ? property.hashCode() : 0;
        result = 31 * result + (value != null ? value.hashCode() : 0);
        return result;
    }
}
