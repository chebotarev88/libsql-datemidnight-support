package net.sf.jparts.libsql.query.client;

import javax.xml.bind.annotation.*;

@XmlRootElement(name = "not")
@XmlAccessorType(XmlAccessType.FIELD)
public class NotConstraint implements Constraint {

    private static final long serialVersionUID = -3655404398356147914L;

    @XmlElementRefs({
            @XmlElementRef(type = AndConstraint.class)
            ,@XmlElementRef(type = ConjunctionConstraint.class)
            ,@XmlElementRef(type = OrConstraint.class)
            ,@XmlElementRef(type = DisjunctionConstraint.class)
            ,@XmlElementRef(type = SimpleConstraint.class)
            ,@XmlElementRef(type = ContainsConstraint.class)
            ,@XmlElementRef(type = NullConstraint.class)
            ,@XmlElementRef(type = NotNullConstraint.class)
            ,@XmlElementRef(type = NotConstraint.class)
            ,@XmlElementRef(type = InConstraint.class)
            ,@XmlElementRef(type = InIgnoreCaseConstraint.class)
            ,@XmlElementRef(type = BetweenConstraint.class)
            ,@XmlElementRef(type = StartsWithConstraint.class)
            ,@XmlElementRef(type = StartsWithCaseSensitiveConstraint.class)
            ,@XmlElementRef(type = EndsWithConstraint.class)
            ,@XmlElementRef(type = EndsWithCaseSensitiveConstraint.class)
            ,@XmlElementRef(type = EqualsIgnoreCaseConstraint.class)
    })
    private Constraint constraint;

    public NotConstraint() {
    }

    public NotConstraint(Constraint constraint) {
        this.constraint = constraint;
    }

    public Constraint getConstraint() {
        return constraint;
    }

    public void setConstraint(Constraint constraint) {
        this.constraint = constraint;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        NotConstraint that = (NotConstraint) o;

        //noinspection RedundantIfStatement
        if (constraint != null ? !constraint.equals(that.constraint) : that.constraint != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return constraint != null ? constraint.hashCode() : 0;
    }
}
