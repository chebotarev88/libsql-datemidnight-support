package net.sf.jparts.libsql.mapping.types;

import java.sql.ResultSet;
import java.sql.SQLException;

public class StringTypeHandler implements TypeHandler<String> {

    @Override
    public String getResult(ResultSet rs, String column) throws SQLException {
        return rs.getString(column);
    }
}
