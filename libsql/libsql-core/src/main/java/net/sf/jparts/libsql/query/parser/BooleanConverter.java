package net.sf.jparts.libsql.query.parser;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class BooleanConverter implements ValueConverter<Boolean> {

    private Logger logger = LoggerFactory.getLogger(ValueConverter.LOGGER_NAME);

    private final Boolean unsupportedValue;

    private final Boolean nullValue;

    public BooleanConverter() {
        this(null, null);
    }

    public BooleanConverter(Boolean unsupportedValue, Boolean nullValue) {
        this.nullValue = nullValue;
        this.unsupportedValue = unsupportedValue;
    }

    @Override
    public Boolean convert(String value) {
        if (value == null) return nullValue;
        
        String v = value.trim();
        if (v.isEmpty()) return nullValue;

        if ("true".equalsIgnoreCase(v)
                || "1".equals(v)
                || "yes".equalsIgnoreCase(v)
                || "on".equalsIgnoreCase(v)) {
            return Boolean.TRUE;
        } else if ("false".equalsIgnoreCase(v)
                || "0".equals(v)
                || "no".equalsIgnoreCase(v)
                || "off".equalsIgnoreCase(v)) {
            return Boolean.FALSE;
        }
        
        logger.debug("Can't convert, value = {}", value);
        return unsupportedValue;
    }
}
