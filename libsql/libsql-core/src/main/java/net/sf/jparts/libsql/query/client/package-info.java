@XmlSchema(namespace = "http://jparts.sf.net/libsql/query/client", elementFormDefault = XmlNsForm.QUALIFIED,
        xmlns = @XmlNs(prefix = "qc", namespaceURI = "http://jparts.sf.net/libsql/query/client")
)
package net.sf.jparts.libsql.query.client;

import javax.xml.bind.annotation.XmlNs;
import javax.xml.bind.annotation.XmlNsForm;
import javax.xml.bind.annotation.XmlSchema;