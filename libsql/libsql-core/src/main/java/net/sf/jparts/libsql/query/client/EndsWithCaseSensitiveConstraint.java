package net.sf.jparts.libsql.query.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "endsWithCs")
@XmlAccessorType(XmlAccessType.FIELD)
public class EndsWithCaseSensitiveConstraint extends EndsWithConstraint {

    public EndsWithCaseSensitiveConstraint() {
    }

    public EndsWithCaseSensitiveConstraint(String property, String value) {
        super(property, value);
    }
}
