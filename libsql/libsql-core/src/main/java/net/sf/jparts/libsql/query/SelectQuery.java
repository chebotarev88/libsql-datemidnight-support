package net.sf.jparts.libsql.query;

import net.sf.jparts.libsql.query.constraints.Constraint;
import net.sf.jparts.libsql.query.constraints.Order;

import java.util.List;
import java.util.Map;
import java.util.Set;

public interface SelectQuery<E> {

    public Class<E> getResultClass();

    public String getName();

    public String getQueryString();

    /**
     * The position of the first result the query object was set to
     * retrieve. Returns 0 if <code>setFirstResult</code> was not applied to the
     * query object.
     *
     * @return position of the first result
     */
    public int getFirstResult();

    /**
     * Set the position of the first result to retrieve.
     * @param firstResult position of the first result, numbered from 0
     * @return the same query instance
     * @throws IllegalArgumentException if the argument is negative
     */
    public SelectQuery<E> setFirstResult(int firstResult);

    /**
     * The maximum number of results the query object was set to
     * retrieve. Returns <code>Integer.MAX_VALUE</code> if <code>setMaxResults</code> was not
     * applied to the query object.
     *
     * @return maximum number of results
     */
    public int getMaxResults();

    /**
     * Set the maximum number of results to retrieve.
     * @param maxResults maximum number of results to retrieve
     * @return the same query instance
     * @throws IllegalArgumentException if the argument is negative
     */
    public SelectQuery<E> setMaxResults(int maxResults);

    /**
     * Return Map with hint names and values defined for current <code>SelectQuery</code> instance.
     * Changes for this map must not reflect <code>SelectQuery</code> instance.
     *
     * @return mutable map, not <code>null</code>, empty map if there is no hints defined.
     */
    public Map<String, Object> getHints();

    public Object getHintValue(String hintName);

    public SelectQuery<E> setHint(String hintName, Object value);

    public SelectQuery<E> setParameter(int position, Object value);

    public SelectQuery<E> setParameter(String name, Object value);

    public Set<QueryParameter<?>> getParameters();

    public <T> T getParameterValue(QueryParameter<T> param);

    public SelectQuery<E> orderBy(List<Order> orders);

    public SelectQuery<E> orderBy(Order... orders);

    /**
     * Return list of orders defined for query. Changes for this list must not reflect query instance.
     * @return mutable list, not <code>null</code>, empty map if order is undefined.
     */
    public List<Order> getOrderBy();

    public SelectQuery<E> where(List<Constraint> constraints);

    public SelectQuery<E> where(Constraint... constraints);

    public List<Constraint> getConstraints();
}
