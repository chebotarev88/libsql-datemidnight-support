package net.sf.jparts.libsql.query.constraints;

public class EndsWithCaseSensitiveConstraint extends EndsWithConstraint {

    protected EndsWithCaseSensitiveConstraint(String propertyName, Object value) {
        super(propertyName, value);
    }

    @Override
    public String toString() {
        return getPropertyName() + " like %" + getValue();
    }

}
