package net.sf.jparts.libsql.loader;


import java.util.Iterator;
import java.util.ServiceLoader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Used to get {@link QueryLoader} instance. Creates loader during static initialization. 
 */
public abstract class QueryLoaderService {

    private static final Logger logger = LoggerFactory.getLogger(QueryLoaderService.class);

    private static QueryLoader loader;

    static {
        ServiceLoader<QueryLoader> serviceLoader = ServiceLoader.load(QueryLoader.class);
        Iterator<QueryLoader> it = serviceLoader.iterator();
        if (it.hasNext()) {
            loader = it.next();
            logger.debug("QueryLoader initialized ({})", loader.getClass());
        } else {
            logger.error("QueryLoader is not configured. " +
                    "Check META-INF/services/net.sf.jparts.libsql.loader.QueryLoader");
        }
    }

    /**
     * Returns current {@link QueryLoader} instance.
     *
     * @return not <code>null</code>.
     */
    public static QueryLoader getLoader() {
        return loader;
    }

    /**
     * Constructor is private because it is not necessary to create instances of this class.
     */
    private QueryLoaderService() {
        // do nothing
    }
}
