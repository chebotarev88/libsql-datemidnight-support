package net.sf.jparts.libsql.query.util;

import net.sf.jparts.libsql.query.SelectQuery;
import net.sf.jparts.libsql.query.constraints.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SelectQueryDefaultTransformer implements SelectQueryTransformer {

    public static final String LINE_SEPARATOR_VALUE_HINT = "SelectQueryDefaultTransformer.lineSeparatorValue";

    private String namedParamPrefix = "#";

    public SelectQueryDefaultTransformer(String namedParamPrefix) {
        if (namedParamPrefix == null || namedParamPrefix.trim().length() < 1) {
            throw new IllegalArgumentException("namedParamPrefix must not be empty string or null");
        }
        this.namedParamPrefix = namedParamPrefix;
    }

    public <E> TransformationResult buildQueryString(SelectQuery<E> query) {
        return defaultTransformation(query);
    }

    protected <E> TransformationResult defaultTransformation(SelectQuery<E> query) {
        TransformationResult result = new TransformationResult(query.getQueryString(), getLineSeparator(query));

        applyConstraints(result, query);
        applyOrderBy(result.getQueryBuilder(), query);
        return result;
    }

    protected <E> void applyConstraints(TransformationResult result, SelectQuery<E> query) {
        List<Constraint> constraints = query.getConstraints();
        if (constraints.isEmpty()) {
            return;
        }

        if (shouldWrapForConstraints(result, query)) {
            wrapForConstraints(result, query);
        }

        applyConstraints(result.getQueryBuilder(), result, constraints);
    }

    protected void applyConstraints(StringBuilder sb, TransformationResult result, List<Constraint> constraints) {
        if (constraints.isEmpty()) return;

        sb.append(result.getLineSeparator());
        sb.append(" where ");

        Map<ParameterInfo, Object> namedParams = new HashMap<>();
        int j = result.getParamCounter();
        for (int i = 0; i < constraints.size(); i++) {
            if (i > 0) {
                sb.append(" and ");
            }
            j = constraintToString(constraints.get(i), j, sb, namedParams);
            result.setParamCounter(j);
        }

        result.getNamedParams().putAll(namedParams);
    }

    protected <E> void wrapForConstraints(TransformationResult result, SelectQuery<E> query) {
        //noinspection MismatchedQueryAndUpdateOfStringBuilder
        StringBuilder sb = result.getQueryBuilder();

        sb.insert(0, "select * from ( " + result.getLineSeparator());
        sb.append(" )");
    }

    protected <E> boolean shouldWrapForConstraints(TransformationResult result, SelectQuery<E> query) {
        return false; // false by default (for backwards compatibility and for jpa finders)
    }

    protected <E> String getLineSeparator(SelectQuery<E> query) {
        Object o = query.getHintValue(LINE_SEPARATOR_VALUE_HINT);
        if (o == null || !(o instanceof String)) {
            return "\n"; // unix like by default
        }
        return (String) o;
    }

    protected int constraintToString(Constraint c, int j, StringBuilder sb, Map<ParameterInfo, Object> namedParams) {
        if (c instanceof NotConstraint) {
            return notConstraintToString((NotConstraint) c, j, sb, namedParams);
        } else if (c instanceof SimpleConstraint) {
            return simpleConstraintToString((SimpleConstraint) c, j, sb, namedParams);
        } else if (c instanceof BetweenConstraint) {
            return betweenConstraintToString((BetweenConstraint) c, j, sb, namedParams);
        } else if (c instanceof AbstractJoinConstraint) {
            return joinConstraintToString((AbstractJoinConstraint) c, j, sb, namedParams);
        } else if (c instanceof InIgnoreCaseConstraint) {
            return inIgnoreCaseConstraintToString((InIgnoreCaseConstraint) c, j, sb, namedParams);
        } else if (c instanceof InConstraint) {
            return inConstraintToString((InConstraint) c, j, sb, namedParams);
        } else if (c instanceof ContainsConstraint) {
            return containsConstraintToString((ContainsConstraint) c, j, sb, namedParams);
        } else if (c instanceof EqualsIgnoreCaseConstraint) {
            return equalsIgnoreCaseConstraintToString((EqualsIgnoreCaseConstraint) c, j, sb, namedParams);
        } else if(c instanceof StartsWithCaseSensitiveConstraint) {
            return startsWithCaseSensitiveConstraintToString((StartsWithCaseSensitiveConstraint) c, j, sb, namedParams);
        } else if (c instanceof StartsWithConstraint) {
            return startsWithConstraintToString((StartsWithConstraint) c, j, sb, namedParams);
        } else if(c instanceof EndsWithCaseSensitiveConstraint) {
            return endsWithCaseSensitiveConstraintToString((EndsWithCaseSensitiveConstraint) c, j, sb, namedParams);
        } else if (c instanceof EndsWithConstraint) {
            return endsWithConstraintToString((EndsWithConstraint) c, j, sb, namedParams);
        } else if (c instanceof PropertyConstraint) {
            return propertyConstraintToString((PropertyConstraint) c, j, sb);
        }

        sb.append(c.toString());
        return j;
    }

    protected int propertyConstraintToString(PropertyConstraint c, int j, StringBuilder sb) {
        sb.append(getPropertyName(c.getPropertyName()));
        sb.append(" ").append(c.getOperator()).append(" ");
        sb.append(getPropertyName(c.getOtherPropertyName()));
        return j;
    }

    protected int notConstraintToString(NotConstraint nc, int j, StringBuilder sb, Map<ParameterInfo, Object> namedParams) {
        sb.append("not (");
        int x = constraintToString(nc.getConstraint(), j, sb, namedParams);
        sb.append(")");
        return x;
    }

    protected String createNamedParam(String propertyName, int j, Object value, Map<ParameterInfo, Object> namedParams) {
        String paramName = "param_" + propertyName + "_" + j;
        ParameterInfo pi = new ParameterInfo(propertyName, paramName);
        namedParams.put(pi, value);

        return namedParamPrefix + paramName;
    }

    protected int simpleConstraintToString(SimpleConstraint c, int j, StringBuilder sb, Map<ParameterInfo, Object> namedParams) {
        sb.append(getPropertyName(c));
        sb.append(" ");
        sb.append(c.getOperator());
        sb.append(" ");

        String paramName = createNamedParam(c.getPropertyName(), j, c.getValue(), namedParams);
        sb.append(paramName);

        return j + 1;
    }

    protected int betweenConstraintToString(BetweenConstraint c, int j, StringBuilder sb, Map<ParameterInfo, Object> namedParams) {
        sb.append(getPropertyName(c));
        sb.append(" between ");

        String p = createNamedParam(c.getPropertyName(), j, c.getLow(), namedParams);
        sb.append(p);

        sb.append(" and ");

        p = createNamedParam(c.getPropertyName(), j + 1, c.getHigh(), namedParams);
        sb.append(p);

        return j + 2;
    }

    protected int joinConstraintToString(AbstractJoinConstraint c, int j, StringBuilder sb, Map<ParameterInfo, Object> namedParams) {
        int n = j;

        List<Constraint> constraints = c.getConstraints();
        if (constraints.size() > 0) {
            if (c.isGroup()) {
                sb.append("(");
            }
            for (int i = 0; i < constraints.size(); i++) {
                if (i > 0) {
                    sb.append((c.isAppendSpaces()) ? " " + c.getJoinOperator() + " " : c.getJoinOperator());
                }
                n = constraintToString(constraints.get(i), n, sb, namedParams);
            }
            if (c.isGroup()) {
                sb.append(")");
            }
        }
        return n;
    }

    protected int inConstraintToString(InConstraint c, int j, StringBuilder sb, Map<ParameterInfo, Object> namedParams) {
        int n = j;
        sb.append(getPropertyName(c));
        sb.append(" in (");

        List<Object> values = c.getValues();
        for (int i = 0; i < values.size(); i++) {
            if (i > 0) {
                sb.append(", ");
            }
            sb.append(createNamedParam(c.getPropertyName(), n++, values.get(i), namedParams));
        }

        sb.append(")");
        return n;
    }

    protected int inIgnoreCaseConstraintToString(InIgnoreCaseConstraint c, int j, StringBuilder sb,
            Map<ParameterInfo, Object> namedParams) {
        int n = j;
        sb.append("UPPER(").append(getPropertyName(c)).append(")");
        sb.append(" in (");

        List<Object> values = c.getValues();
        for (int i = 0; i < values.size(); i++) {
            if (i > 0) {
                sb.append(", ");
            }
            sb.append("UPPER(").append(createNamedParam(c.getPropertyName(), n++, values.get(i), namedParams))
                    .append(")");
        }

        sb.append(")");
        return n;
    }

    protected int containsConstraintToString(ContainsConstraint c, int j, StringBuilder sb, Map<ParameterInfo, Object> namedParams) {
        int n = j;
        sb.append("upper(");
        sb.append(getPropertyName(c));
        sb.append(") like upper(");

        Object value = c.getValue();
        sb.append(createNamedParam(c.getPropertyName(), n++, "%" + value + "%", namedParams));

        sb.append(")");
        return n;
    }

    protected int equalsIgnoreCaseConstraintToString(EqualsIgnoreCaseConstraint c, int j, StringBuilder sb, Map<ParameterInfo, Object> namedParams) {
        int n = j;
        sb.append("upper(");
        sb.append(getPropertyName(c));
        sb.append(") = upper(");

        Object value = c.getValue();
        sb.append(createNamedParam(c.getPropertyName(), n++, value, namedParams));

        sb.append(")");
        return n;
    }

    protected int startsWithConstraintToString(StartsWithConstraint c, int j, StringBuilder sb, Map<ParameterInfo, Object> namedParams) {
        int n = j;
        sb.append("upper(");
        sb.append(getPropertyName(c));
        sb.append(") like upper(");

        Object value = c.getValue();
        sb.append(createNamedParam(c.getPropertyName(), n++, value + "%", namedParams));

        sb.append(")");
        return n;
    }

    protected int startsWithCaseSensitiveConstraintToString(StartsWithCaseSensitiveConstraint c, int j, StringBuilder sb, Map<ParameterInfo, Object> namedParams) {
        int n = j;
        sb.append(getPropertyName(c));
        sb.append(" like ");

        Object value = c.getValue();
        sb.append(createNamedParam(c.getPropertyName(), n++, value + "%", namedParams));

        return n;
    }

    protected int endsWithConstraintToString(EndsWithConstraint c, int j, StringBuilder sb, Map<ParameterInfo, Object> namedParams) {
        int n = j;
        sb.append("upper(");
        sb.append(getPropertyName(c));
        sb.append(") like upper(");

        Object value = c.getValue();
        sb.append(createNamedParam(c.getPropertyName(), n++, "%" + value, namedParams));

        sb.append(")");
        return n;
    }

    protected int endsWithCaseSensitiveConstraintToString(EndsWithCaseSensitiveConstraint c, int j, StringBuilder sb, Map<ParameterInfo, Object> namedParams) {
        int n = j;

        sb.append(getPropertyName(c));
        sb.append(" like ");

        Object value = c.getValue();
        sb.append(createNamedParam(c.getPropertyName(), n++, "%" + value, namedParams));

        return n;
    }

    protected <E> void applyOrderBy(StringBuilder sb, final SelectQuery<E> query) {
        applyOrderBy(sb, query.getOrderBy());
    }

    protected void applyOrderBy(StringBuilder sb, List<Order> orders) {
        if (orders.isEmpty()) return;

        if (orders.size() > 0) {
            sb.append(" order by ");
            for (int i = 0; i < orders.size(); i++) {
                if (i > 0) {
                    sb.append(", ");
                }
                sb.append(orderToString(orders.get(i)));
            }
        }
    }

    protected String orderToString(Order o) {
        return getPropertyName(o) + " " + (o.isAscending() ? "asc" : "desc");
    }

    protected String getPropertyName(OnePropertyConstraint c) {
        return getPropertyName(c.getPropertyName());
    }

    protected String getPropertyName(Order o) {
        return getPropertyName(o.getPropertyName());
    }

    protected String getPropertyName(String propertyName) {
        return propertyName;
    }
}
