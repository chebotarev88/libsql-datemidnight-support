package net.sf.jparts.libsql.query.parser;

import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public final class SqlTimeConverter extends AbstractDateConverter<Time> {

    public static final String DEFAULT_FORMAT = "HH:mm:ss";

    public SqlTimeConverter() {
        super(DEFAULT_FORMAT);
    }

    public SqlTimeConverter(final String pattern) {
        super(pattern);
    }

    public SqlTimeConverter(boolean safe, final String pattern, Time errorValue, Time nullValue) {
        super(safe, pattern, errorValue, nullValue);
    }

    @Override
    protected Time tryParse(String value, SimpleDateFormat format) throws ParseException {
        return new Time(format.parse(value).getTime());
    }
}
