package net.sf.jparts.libsql.mapping;

import java.util.*;

public class ResultMapping {

    private boolean autoMapping = true;

    private String id;

    private Class<?> type;

    private ConstructorMapping constructorMapping;

    private DiscriminatorMapping discriminatorMapping;

    private Map<String, PropertyMapping> propertyMappings;

    private ResultMapping() {
        // use builder
    }

    public boolean isAutoMapping() {
        return autoMapping;
    }

    public String getId() {
        return id;
    }

    public Class<?> getType() {
        return type;
    }

    public ConstructorMapping getConstructorMapping() {
        return constructorMapping;
    }

    public Map<String, PropertyMapping> getPropertyMappings() {
        return propertyMappings;
    }

    public DiscriminatorMapping getDiscriminatorMapping() {
        return discriminatorMapping;
    }

    // builder

    public static class Builder {

        private ResultMapping result = new ResultMapping();

        private Map<String, PropertyMapping> propertyMappings = new HashMap<>();

        public Builder(String id) {
            if (id == null || id.isEmpty()) {
                throw new IllegalArgumentException("id must not be empty or null");
            }
            result.id = id;
        }

        public Builder constructorArguments(List<ArgumentMapping> arguments) {
            result.constructorMapping = new ConstructorMapping(arguments);
            return this;
        }

        public Builder constructorArguments(ArgumentMapping... arguments) {
            result.constructorMapping = new ConstructorMapping(arguments);
            return this;
        }

        public Builder setAutoMapping(boolean autoMapping) {
            result.autoMapping = autoMapping;
            return this;
        }

        public Builder setType(Class<?> type) {
            if (type == null) {
                throw new IllegalArgumentException("type must not be null, id = " + result.id);
            }
            result.type = type;
            return this;
        }

        public Builder setType(String className) {
            if (className == null || className.isEmpty()) {
                throw new IllegalArgumentException("className must not be empty or null, id = " + result.id);
            }
            try {
                result.type = Class.forName(className);
                return this;
            } catch (ClassNotFoundException ex) {
                throw new IllegalArgumentException("id = " + result.id + ", message = " + ex.getMessage(), ex);
            }
        }

        public Builder addPropertyMapping(PropertyMapping mapping) {
            if (mapping == null) {
                throw new IllegalArgumentException("mapping must not be empty or null");
            }
            if (mapping.getName() == null || mapping.getName().isEmpty()) {
                throw new IllegalArgumentException("mapping name must not be empty or null");
            }
            String name = mapping.getName().toLowerCase();
            if (this.propertyMappings.containsKey(name)) {
                throw new IllegalArgumentException("property " + mapping.getName() + " mapped multiple times");
            }
            this.propertyMappings.put(name, mapping);
            return this;
        }

        public Builder addPropertyMappings(Iterable<PropertyMapping> mappings) {
            if (mappings == null) {
                throw new IllegalArgumentException("mappings must not be null");
            }
            for (PropertyMapping pm : mappings) {
                this.addPropertyMapping(pm);
            }
            return this;
        }

        public Builder addPropertyMappings(PropertyMapping... mappings) {
            if (mappings == null) {
                throw new IllegalArgumentException("mappings must not be null");
            }

            for (PropertyMapping pm : mappings) {
                this.addPropertyMapping(pm);
            }
            return this;
        }

        public Builder setDiscriminatorMapping(DiscriminatorMapping discriminator) {
            if (discriminator == null) {
                throw new IllegalArgumentException("discriminator must not be empty or null");
            }
            result.discriminatorMapping = discriminator;
            return this;
        }

        private void validate() {
            if (result.type == null) {
                throw new IllegalStateException("type is not defined for mapping, id = " + result.id);
            }
        }

        public ResultMapping build() {
            validate();

            result.propertyMappings = (this.propertyMappings == null)
                    ? Collections.<String, PropertyMapping>emptyMap()
                    : Collections.unmodifiableMap(this.propertyMappings);
            return result;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ResultMapping mapping = (ResultMapping) o;

        if (autoMapping != mapping.autoMapping) return false;
        if (constructorMapping != null ? !constructorMapping.equals(mapping.constructorMapping) : mapping.constructorMapping != null)
            return false;
        if (discriminatorMapping != null ? !discriminatorMapping.equals(mapping.discriminatorMapping) : mapping.discriminatorMapping != null)
            return false;
        if (id != null ? !id.equals(mapping.id) : mapping.id != null) return false;
        if (propertyMappings != null ? !propertyMappings.equals(mapping.propertyMappings) : mapping.propertyMappings != null)
            return false;
        //noinspection RedundantIfStatement
        if (type != null ? !type.equals(mapping.type) : mapping.type != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (autoMapping ? 1 : 0);
        result = 31 * result + (id != null ? id.hashCode() : 0);
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + (constructorMapping != null ? constructorMapping.hashCode() : 0);
        result = 31 * result + (discriminatorMapping != null ? discriminatorMapping.hashCode() : 0);
        result = 31 * result + (propertyMappings != null ? propertyMappings.hashCode() : 0);
        return result;
    }
}
