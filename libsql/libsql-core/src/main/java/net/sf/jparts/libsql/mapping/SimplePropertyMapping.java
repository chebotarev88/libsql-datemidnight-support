package net.sf.jparts.libsql.mapping;

import net.sf.jparts.libsql.mapping.types.TypeHandler;
import net.sf.jparts.libsql.mapping.types.TypeHandlerRegistry;

public class SimplePropertyMapping extends PropertyMapping {

    private String column;

    private TypeHandler<?> typeHandler;

    @SuppressWarnings("unchecked")
    public SimplePropertyMapping(String name, String column, String typeHandlerClassName) {
        super(name);
        this.column = column;

        if (typeHandlerClassName != null && ! typeHandlerClassName.isEmpty()) {
            this.typeHandler = TypeHandlerRegistry.getInstance().getByName(typeHandlerClassName);
        }
    }

    public SimplePropertyMapping(String name, String column, TypeHandler<?> typeHandler) {
        super(name);
        this.column = column;
        this.typeHandler = typeHandler;
    }

    public String getColumn() {
        return (column == null || column.isEmpty()) ? getName() : column;
    }

    public TypeHandler<?> getTypeHandler() {
        return typeHandler;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        SimplePropertyMapping that = (SimplePropertyMapping) o;

        if (column != null ? !column.equals(that.column) : that.column != null) return false;
        //noinspection RedundantIfStatement
        if (typeHandler != null ? !typeHandler.equals(that.typeHandler) : that.typeHandler != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (column != null ? column.hashCode() : 0);
        result = 31 * result + (typeHandler != null ? typeHandler.hashCode() : 0);
        return result;
    }
}
