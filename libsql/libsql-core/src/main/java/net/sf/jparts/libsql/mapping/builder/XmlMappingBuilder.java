package net.sf.jparts.libsql.mapping.builder;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.ValidationEventHandler;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import net.sf.jparts.libsql.mapping.ArgumentMapping;
import net.sf.jparts.libsql.mapping.AssociationMapping;
import net.sf.jparts.libsql.mapping.DiscriminatorMapping;
import net.sf.jparts.libsql.mapping.NestedArgumentMapping;
import net.sf.jparts.libsql.mapping.PropertyMapping;
import net.sf.jparts.libsql.mapping.ResultMapping;
import net.sf.jparts.libsql.mapping.SimpleArgumentMapping;
import net.sf.jparts.libsql.mapping.SimplePropertyMapping;
import net.sf.jparts.libsql.mapping.builder.xml.ArgumentTag;
import net.sf.jparts.libsql.mapping.builder.xml.AssociationTag;
import net.sf.jparts.libsql.mapping.builder.xml.CaseTag;
import net.sf.jparts.libsql.mapping.builder.xml.DiscriminatorTag;
import net.sf.jparts.libsql.mapping.builder.xml.PropertyTag;
import net.sf.jparts.libsql.mapping.builder.xml.ResultTag;

import org.xml.sax.SAXException;

public class XmlMappingBuilder {

    public static final String MAPPING_XSD = "META-INF/libsql/xsd/libsql-mapping-2.0.xsd";

    protected JAXBContext jaxbContext;

    protected Schema schema;

    protected ValidationEventHandler eventHandler;

    public XmlMappingBuilder() {
        init();
    }

    protected void init() {
        compileSchema();
        createJaxbContext();
        createValidationHandler();
    }

    protected void compileSchema() {
        try {
            SchemaFactory f = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            schema = f.newSchema(getResource(MAPPING_XSD));
        } catch (SAXException ex) {
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }

    protected void createJaxbContext() {
        try {
            jaxbContext = JAXBContext.newInstance(ResultTag.class);
        } catch (JAXBException ex) {
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }

    private void createValidationHandler() {
        eventHandler = new XmlValidationEventHandler();
    }

    protected URL getResource(String name) {
        URL url = Thread.currentThread().getContextClassLoader().getResource(name);
        if (url == null) {
            throw new XmlParsingException("Can't read resource " + name);
        }
        return url;
    }

    public ResultMapping build(String id) {
        if (id == null || id.isEmpty()) {
            throw new IllegalArgumentException("result id must not be empty or null");
        }
        return buildMapping(parse(id), id);
    }

    protected String getFileName(String id) {
        return "META-INF/libsql/mapping/" + id + ".xml";
    }

    protected ResultTag parse(String id) {
        try {
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            unmarshaller.setSchema(schema);
            unmarshaller.setEventHandler(eventHandler);

            return (ResultTag) unmarshaller.unmarshal(getResource(getFileName(id)));
        } catch (JAXBException ex) {
            throw new XmlParsingException(ex.getMessage(), ex);
        }
    }

    protected ResultMapping buildMapping(ResultTag resultTag, String id) {
        try {
            if (! id.equals(resultTag.id)) {
                throw new XmlParsingException("mapping id incorrect, actual = " + resultTag.id + ", expected = " + id);
            }
            ResultMapping.Builder b = new ResultMapping.Builder(resultTag.id)
                    .setType(resultTag.type);

            if (resultTag.autoMapping != null) {
                b.setAutoMapping(resultTag.autoMapping);
            }

            buildConstructorMapping(resultTag, b);
            buildDiscriminatorMapping(resultTag, b);
            buildPropertiesMappings(resultTag, b);
            buildAssociationsMappings(resultTag, b);

            return b.build();
        } catch (IllegalArgumentException | IllegalStateException | XmlParsingException ex) {
            throw new XmlParsingException("exception while building mapping for id = "
                    + resultTag.id + ", message = " + ex.getMessage(), ex);
        }
    }

    protected void buildConstructorMapping(ResultTag resultTag, ResultMapping.Builder builder) {
        if (resultTag.constructor == null) {
            return;
        }

        List<ArgumentMapping> arguments = new ArrayList<>();
        for (ArgumentTag a : resultTag.constructor.arguments) {
            arguments.add(buildArgumentMapping(a, resultTag));
        }

        builder.constructorArguments(arguments);
    }

    protected ArgumentMapping buildArgumentMapping(ArgumentTag a, ResultTag resultTag) {
        return (empty(a.result))
                ? buildSimpleArgument(a, resultTag)
                : buildNestedArgument(a, resultTag);
    }

    protected ArgumentMapping buildSimpleArgument(ArgumentTag a, ResultTag resultTag) {
        if (! empty(a.columnPrefix)) {
            throw new XmlParsingException("columnPrefix must not be set for constructor " +
                    "argument without reference to result, result id = " + resultTag.id);
        }
        SimpleArgumentMapping.Builder sb = new SimpleArgumentMapping.Builder(a.column)
                .setType(a.type);
        if (! empty(a.typeHandler)) {
            sb.setTypeHandler(a.typeHandler);
        }
        return sb.build();
    }

    protected ArgumentMapping buildNestedArgument(ArgumentTag a, ResultTag resultTag) {
        checkNestedArgumentAttribute(a.column, "column");
        checkNestedArgumentAttribute(a.typeHandler, "typeHandler");

        NestedArgumentMapping.Builder nb = new NestedArgumentMapping.Builder(a.result)
                .setType(a.type);
        if (! empty(a.columnPrefix)) {
            nb.setColumnPrefix(a.columnPrefix);
        }
        return nb.build();
    }

    protected void checkNestedArgumentAttribute(String value, String name) {
        if (! empty(value)) {
            throw new XmlParsingException("Attributes result and " + name + " can not be used simultaneously");
        }
    }

    protected void buildDiscriminatorMapping(ResultTag resultTag, ResultMapping.Builder builder) {
        if (resultTag.discriminator == null) {
            return;
        }

        DiscriminatorTag dtag = resultTag.discriminator;
        DiscriminatorMapping.Builder db = new DiscriminatorMapping.Builder(dtag.column);
        for (CaseTag ctag : dtag.cases) {
            db.addCase(ctag.value, ctag.result);
        }

        builder.setDiscriminatorMapping(db.build());
    }

    protected void buildPropertiesMappings(ResultTag resultTag, ResultMapping.Builder builder) {
        if (resultTag.properties == null) {
            return;
        }

        for (PropertyTag p : resultTag.properties) {
            builder.addPropertyMapping(new SimplePropertyMapping(p.name, p.column, p.typeHandler));
        }
    }

    protected void buildAssociationsMappings(ResultTag resultTag, ResultMapping.Builder builder) {
        if (resultTag.associations == null) {
            return;
        }

        for (AssociationTag a : resultTag.associations) {
            builder.addPropertyMapping(buildAssociationMapping(a));
        }
    }

    protected PropertyMapping buildAssociationMapping(AssociationTag atag) {
        AssociationMapping.Builder b = new AssociationMapping.Builder(atag.name, atag.result);
        if (! empty(atag.columnPrefix)) {
            b.setColumnPrefix(atag.columnPrefix);
        }
        return b.build();
    }

    protected boolean empty(String s) {
        return (s == null) || (s.trim().isEmpty());
    }

}
