package net.sf.jparts.libsql.query.parser;

import java.math.BigDecimal;

public final class BigDecimalConverter extends AbstractDecimalConverter<BigDecimal> {

    public BigDecimalConverter() {
    }

    public BigDecimalConverter(boolean safe, BigDecimal errorValue, BigDecimal nullValue) {
        super(safe, errorValue, nullValue);
    }

    @Override
    protected BigDecimal tryCreate(String value) throws NumberFormatException {
        return new BigDecimal(value);
    }
}
