package net.sf.jparts.libsql.query.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "between")
@XmlAccessorType(XmlAccessType.FIELD)
public class BetweenConstraint implements Constraint {

    private static final long serialVersionUID = -8824021392049670754L;

    @XmlElement(required = true)
    private String property;

    @XmlElement(required = true)
    private String low;

    @XmlElement(required = true)
    private String high;

    public BetweenConstraint() {
    }

    public BetweenConstraint(String property, String low, String high) {
        this.property = property;
        this.low = low;
        this.high = high;
    }

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    public String getLow() {
        return low;
    }

    public void setLow(String low) {
        this.low = low;
    }

    public String getHigh() {
        return high;
    }

    public void setHigh(String high) {
        this.high = high;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BetweenConstraint that = (BetweenConstraint) o;

        if (high != null ? !high.equals(that.high) : that.high != null) return false;
        if (low != null ? !low.equals(that.low) : that.low != null) return false;
        //noinspection RedundantIfStatement
        if (property != null ? !property.equals(that.property) : that.property != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = property != null ? property.hashCode() : 0;
        result = 31 * result + (low != null ? low.hashCode() : 0);
        result = 31 * result + (high != null ? high.hashCode() : 0);
        return result;
    }
}
