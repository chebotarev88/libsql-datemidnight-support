package net.sf.jparts.libsql.mapping.types;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ShortTypeHandler implements TypeHandler<Short> {

    @Override
    public Short getResult(ResultSet rs, String column) throws SQLException {
        short s = rs.getShort(column);
        return (rs.wasNull()) ? null : s;
    }
}
