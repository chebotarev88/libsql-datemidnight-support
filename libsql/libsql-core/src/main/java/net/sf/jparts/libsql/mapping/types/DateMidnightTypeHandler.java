package net.sf.jparts.libsql.mapping.types;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.joda.time.DateMidnight;

@Deprecated
@SuppressWarnings("deprecation")
public class DateMidnightTypeHandler implements TypeHandler<DateMidnight> {

    @Override
    public DateMidnight getResult(ResultSet rs, String column) throws SQLException {
        Date date = rs.getDate(column);
        return (date == null) ? null : new DateMidnight(date);
    }
}