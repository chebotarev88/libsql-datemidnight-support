package net.sf.jparts.libsql.mapping;

public class AssociationMapping extends PropertyMapping {

    private String columnPrefix;

    private ResultMapping nested;

    protected AssociationMapping(String name) {
        super(name);
    }

    public String getColumnPrefix() {
        return (columnPrefix == null) ? "" : columnPrefix;
    }

    public ResultMapping getNested() {
        return nested;
    }

    public static class Builder {

        private AssociationMapping result;

        public Builder(String name, String nestedId) {
            if (name == null || name.isEmpty()) {
                throw new IllegalArgumentException("name must not be empty or null");
            }
            result = new AssociationMapping(name);
            result.nested = MappingFactory.getInstance().create(nestedId);
        }

        public Builder(String name, ResultMapping nestedMapping) {
            if (name == null || name.isEmpty()) {
                throw new IllegalArgumentException("name must not be empty or null");
            }
            if (nestedMapping == null) {
                throw new IllegalArgumentException("nestedMapping must not be null");
            }
            result = new AssociationMapping(name);
            result.nested = nestedMapping;
        }

        public Builder setColumnPrefix(String columnPrefix) {
            if (columnPrefix == null || columnPrefix.isEmpty()) {
                throw new IllegalArgumentException("columnPrefix must not be empty or null");
            }
            result.columnPrefix = columnPrefix;
            return this;
        }

        private void validate() {
            if (result.nested == null) {
                throw new IllegalStateException("nested mapping not defined for argument");
            }
        }

        public AssociationMapping build() {
            validate();
            return result;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        AssociationMapping that = (AssociationMapping) o;

        if (columnPrefix != null ? !columnPrefix.equals(that.columnPrefix) : that.columnPrefix != null) return false;
        //noinspection RedundantIfStatement
        if (nested != null ? !nested.equals(that.nested) : that.nested != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (columnPrefix != null ? columnPrefix.hashCode() : 0);
        result = 31 * result + (nested != null ? nested.hashCode() : 0);
        return result;
    }
}
