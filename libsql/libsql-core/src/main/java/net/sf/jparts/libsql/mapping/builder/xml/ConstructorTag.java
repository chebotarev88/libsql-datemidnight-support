package net.sf.jparts.libsql.mapping.builder.xml;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ConstructorMapping", propOrder = {
    "arguments"
})
public class ConstructorTag {

    @XmlElement(required = true, name = "argument")
    public List<ArgumentTag> arguments;

}
