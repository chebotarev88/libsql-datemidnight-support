package net.sf.jparts.libsql.query.client;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "inIgnoreCase")
@XmlAccessorType(XmlAccessType.FIELD)
public class InIgnoreCaseConstraint extends InConstraint {

    public InIgnoreCaseConstraint() {
    }

    public InIgnoreCaseConstraint(String property) {
        super(property);
    }

    public InIgnoreCaseConstraint(String property, List<String> values) {
        super(property, values);
    }

    public InIgnoreCaseConstraint(String property, String... values) {
        super(property, values);
    }

}
