package net.sf.jparts.libsql.mapping.types;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

public class DateTypeHandler implements TypeHandler<Date> {

    @Override
    public Date getResult(ResultSet rs, String column) throws SQLException {
        return rs.getTimestamp(column);
    }
}
