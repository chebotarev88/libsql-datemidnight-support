package net.sf.jparts.libsql.query.constraints;

/**
 * <code>(constraint1 or constraint2 or ... constraintN)</code>
 */
public class DisjunctionConstraint extends OrConstraint {

    public DisjunctionConstraint() {
        group = true;
    }
}
