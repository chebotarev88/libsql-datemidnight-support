package net.sf.jparts.libsql.query.parser;

import net.sf.jparts.libsql.query.SelectQuery;
import net.sf.jparts.libsql.query.client.QueryConfig;

/**
 * Used for configuring {@link SelectQuery} instances from
 * externally-defined {@link QueryConfig}.
 * Implementations are not limited in how to convert query
 * parameters, constraints and paging parameters.
 * Implementations can be thread-safe, but not required.
 *
 * <p>
 * Typical usage:
 * <pre>
 *  SelectQuery<Bean> sq = LibSql.makeSelectQuery("listBeans", Bean.class);
 *  QueryParser p = new SomeQueryParserImpl();
 *  p.parse(sq, config);
 *  FinderResult<Bean> result = LibSql.find(sq);
 * </pre>
 *
 * You may find {@link ValueConverter} and its implementations in current package
 * useful for creating your own {@code QueryParser}.
 *
 * @since 1.1.0
 */
public interface QueryParser {

    /**
     * Configures given {@link SelectQuery} from external <code>config</code>.
     *
     * @param config external query configuration, not <code>null</code>.
     * @param query initialized query instance, not <code>null</code>.
     * @param <E> type of objects, selected by <code>query</code>.
     *
     * @return <code>query</code> parameter.
     *
     * @throws IllegalArgumentException if any parameter is <code>null</code>.
     */
    public <E> SelectQuery<E> parse(QueryConfig config, SelectQuery<E> query);
}
