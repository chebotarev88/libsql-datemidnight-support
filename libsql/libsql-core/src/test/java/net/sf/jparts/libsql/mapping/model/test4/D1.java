package net.sf.jparts.libsql.mapping.model.test4;

public class D1 extends D {

    private String d1;

    public D1(String value, String d1) {
        super(value);
        this.d1 = d1;
    }

    public String getD1() {
        return d1;
    }

    @Override
    public String toString() {
        return "D1{" +
                "d1='" + d1 + '\'' +
                "} " + super.toString();
    }
}
