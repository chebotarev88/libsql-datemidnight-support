package net.sf.jparts.libsql.mapping.model.test3;

public class B extends A {

    private String b;

    public String getB() {
        return b;
    }

    @Override
    public String toString() {
        return "B{" +
                "b='" + b + '\'' +
                "} " + super.toString();
    }
}
