package net.sf.jparts.libsql.finder;

import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class FinderRegistryTest {

    @Test
    public void testGetInstance() {
        // check getInstance
        FinderRegistry r = FinderRegistry.getInstance();
        Assert.assertNotNull(r);

        // check r is same object as r2
        FinderRegistry r2 = FinderRegistry.getInstance();
        Assert.assertEquals(r.hashCode(), r2.hashCode());        
    }

    @Test
    public void testGetNames() {
        List<String> names = FinderRegistry.getInstance().getNames();
        Assert.assertNotNull(names);
        Assert.assertEquals(names.size(), 2);
        Assert.assertTrue(names.contains("default"));
        Assert.assertTrue(names.contains("tf"));
    }

    @Test
    public void testGet() {
        FinderRegistry r = FinderRegistry.getInstance();
        
        // get default jpa finder
        Finder f = r.get("default");
        Assert.assertNotNull(f);
        Assert.assertTrue(f instanceof TestFinder);

        // get test finder
        f = r.get("tf");
        Assert.assertNotNull(f);
        Assert.assertTrue(f instanceof TestFinder);

        // checks for invalid arguments
        try {
            r.get(null);
            Assert.fail("IllegalArgumentException expected");
        } catch (IllegalArgumentException ex) {
            // it's ok
        }

        try {
            r.get("kwqejn");
            Assert.fail("FinderNotFoundException expected");
        } catch (FinderNotFoundException ex) {
            // it's ok
        }
    }

    @Test
    public void testAddRemove() {
        FinderRegistry r = FinderRegistry.getInstance();
        Assert.assertEquals(r.getNames().size(), 2);

        // add new finder
        Finder f1 = new TestFinder();
        r.add("f1", f1);
        Assert.assertEquals(r.getNames().size(), 3);

        // and remove it
        r.remove("f1");
        Assert.assertEquals(r.getNames().size(), 2);

        // checks for invalid arguments
        try {
            r.remove(null);
            Assert.fail("IllegalArgumentException expected");
        } catch (IllegalArgumentException ex) {
            // it's ok
        }

        try {
            r.add(null, f1);
            Assert.fail("IllegalArgumentException expected");
        } catch (IllegalArgumentException ex) {
            // it's ok
        }

        try {
            r.add("f1", null);
            Assert.fail("IllegalArgumentException expected");
        } catch (IllegalArgumentException ex) {
            // it's ok
        }

    }

}
