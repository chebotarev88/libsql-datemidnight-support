package net.sf.jparts.libsql;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.StringReader;
import java.io.StringWriter;

abstract public class AbstractMarshallTest {

    protected String marshall(Object o) {
        if (o == null) {
            throw new IllegalArgumentException("o must not be null");
        }
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(o.getClass());
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

//            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            jaxbMarshaller.setProperty(Marshaller.JAXB_FRAGMENT, true);

            StringWriter out = new StringWriter();
            jaxbMarshaller.marshal(o, out);

//            jaxbMarshaller.setProperty("eclipselink.media-type", "application/json");
//            jaxbMarshaller.marshal(o, System.out);

            return out.getBuffer().toString();
        } catch (JAXBException ex) {
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }

    @SuppressWarnings("unchecked")
    protected <E> E unmarshal(String xml, Class<E> clazz) throws JAXBException {
        JAXBContext jaxbContext = JAXBContext.newInstance(clazz);
        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

        StringReader reader = new StringReader(xml);
        return (E) jaxbUnmarshaller.unmarshal(reader);
    }

}
