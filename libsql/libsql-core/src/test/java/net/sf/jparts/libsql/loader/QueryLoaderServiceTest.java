package net.sf.jparts.libsql.loader;

import org.junit.Assert;
import org.junit.Test;

public class QueryLoaderServiceTest {

    @Test
    public void testGetLoader() {
        QueryLoader l = QueryLoaderService.getLoader();
        Assert.assertNotNull(l);
        Assert.assertEquals(TestQueryLoader.class.getName(), l.getClass().getName());
    }
}
