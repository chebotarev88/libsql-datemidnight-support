package net.sf.jparts.libsql.mapping.builder.xml;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class ResultXmlUnmarshallTest {

    private InputStream open(String file) {
        return Thread.currentThread().getContextClassLoader().getResourceAsStream(file);
    }

    private ResultTag unmarshall(String xml) throws JAXBException, IOException {
        JAXBContext jaxbContext = JAXBContext.newInstance(ResultTag.class);
        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

        try (InputStream in = open(xml); Reader reader = new InputStreamReader(in)) {
            return (ResultTag) jaxbUnmarshaller.unmarshal(reader);
        }
    }

    @Test
    public void unmarshallTest1() throws Exception {
        ResultTag r = unmarshall("META-INF/libsql/mapping/xsd-test1-correct.xml");

        // header
        assertNotNull(r);
        assertEquals("xsd-test1-correct", r.id);
        assertEquals("java.lang.Object", r.type);
        assertTrue(r.autoMapping);

        // constructor
        assertNotNull(r.constructor);
        assertEquals(1, r.constructor.arguments.size());

        ArgumentTag a = r.constructor.arguments.get(0);
        assertNotNull(a);
        assertEquals("arg_column", a.column);
        assertEquals("arg_type", a.type);
        assertEquals("arg_handler", a.typeHandler);
        assertEquals("arg_result", a.result);
        assertEquals("arg_prefix", a.columnPrefix);

        // discriminator
        assertNotNull(r.discriminator);
        assertEquals(2, r.discriminator.cases.size());

        CaseTag c1 = r.discriminator.cases.get(0);
        assertNotNull(c1);
        assertEquals("disc_v1", c1.value);
        assertEquals("r1", c1.result);

        CaseTag c2 = r.discriminator.cases.get(1);
        assertNotNull(c2);
        assertEquals("disc_v2", c2.value);
        assertEquals("r2", c2.result);

        // properties
        assertNotNull(r.properties);
        assertEquals(2, r.properties.size());

        PropertyTag p1 = r.properties.get(0);
        assertNotNull(p1);
        assertEquals("prop1", p1.name);
        assertEquals("p1_column", p1.column);
        assertEquals("p1_handler", p1.typeHandler);

        PropertyTag p2 = r.properties.get(1);
        assertNotNull(p2);
        assertEquals("prop2", p2.name);
        assertEquals("p2_column", p2.column);
        assertEquals("p2_handler", p2.typeHandler);

        // associations
        assertNotNull(r.associations);
        assertEquals(1, r.associations.size());

        AssociationTag at = r.associations.get(0);
        assertNotNull(at);
        assertEquals("prop3", at.name);
        assertEquals("p3_result", at.result);
        assertEquals("p3_prefix", at.columnPrefix);
    }
}
