package net.sf.jparts.libsql.query.constraints;

import java.util.List;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Unit test for {@link InConstraint}.
 */
public class InConstraintTest {

    private static InConstraint ic;

    @BeforeClass
    public static void setUpBeforeClass() {
        ic = new InConstraint("p", 1, 2, 3);
    }

    @AfterClass
    public static void tearDownAfrterClass() {
        ic = null;
    }

    @Test
    public void testConstructor() {
        try {
            new InConstraint(null);
            fail("IllegalArgumentException expected");
        } catch (IllegalArgumentException ex) {
            // it's ok
        }

        try {
            new InConstraint(" ");
            fail("IllegalArgumentException expected");
        } catch (IllegalArgumentException ex) {
            // it's ok
        }

        try {
            new InConstraint("p");
            fail("IllegalArgumentException expected");
        } catch (IllegalArgumentException ex) {
            // it's ok
        }

        try {
            // noinspection RedundantArrayCreation
            new InConstraint("p", new Object[]{});
            fail("IllegalArgumentException expected");
        } catch (IllegalArgumentException ex) {
            // it's ok
        }
    }
    
    @Test
    public void testGetPropertyName() {
        assertEquals("p", ic.getPropertyName());
    }

    @Test
    public void testGetValues() {
        List<Object> v = ic.getValues();
        assertArrayEquals(v.toArray(), new Object[] {1, 2, 3});
    }

    @Test
    public void testToString() {
        final String ok = "p in (1, 2, 3)";
        assertEquals(ok, ic.toString());
    }
}
