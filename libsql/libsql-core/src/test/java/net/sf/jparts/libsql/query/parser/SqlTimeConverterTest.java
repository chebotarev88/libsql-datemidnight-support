package net.sf.jparts.libsql.query.parser;

import java.sql.Time;
import java.util.Calendar;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class SqlTimeConverterTest {

    @Test
    public void testTimeConvert() {
        SqlTimeConverter dc = new SqlTimeConverter();
        assertNull(dc.convert(null));
        assertNull(dc.convert("str"));
        assertEquals(getTestTime(), dc.convert("10:10:10"));
    }

    @Test
    public void testTimeConvertWith() {
        Time current = getTestTime2();
        SqlTimeConverter dc = new SqlTimeConverter(true, "dd.MM.yyyy", current, current);
        assertEquals(current, dc.convert(null));
        assertEquals(current, dc.convert("str"));
        assertEquals(getTestWithZeroTime(), dc.convert("10.12.2013"));
        assertEquals(current, dc.convert("2013-12-10T10:10:10"));

        dc = new SqlTimeConverter(true, "dd.MM.yyyy HH:mm:ss", current, current);
        assertEquals(current, dc.convert("10.12.2013 10:10:10"));
    }

    private Time getTestWithZeroTime() {
        Calendar cal = Calendar.getInstance();
        cal.set(2013, Calendar.DECEMBER, 10);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return new Time(cal.getTime().getTime());
    }

    private Time getTestTime() {
        Calendar cal = Calendar.getInstance();
        cal.set(1970, Calendar.JANUARY, 1);
        cal.set(Calendar.HOUR_OF_DAY, 10);
        cal.set(Calendar.MINUTE, 10);
        cal.set(Calendar.SECOND, 10);
        cal.set(Calendar.MILLISECOND, 0);
        return new Time(cal.getTime().getTime());
    }

    private Time getTestTime2() {
        Calendar cal = Calendar.getInstance();
        cal.set(2013, Calendar.DECEMBER, 10);
        cal.set(Calendar.HOUR_OF_DAY, 10);
        cal.set(Calendar.MINUTE, 10);
        cal.set(Calendar.SECOND, 10);
        cal.set(Calendar.MILLISECOND, 0);
        return new Time(cal.getTime().getTime());
    }

}

