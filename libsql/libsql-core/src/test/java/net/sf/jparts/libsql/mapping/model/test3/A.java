package net.sf.jparts.libsql.mapping.model.test3;

public class A {

    private String a;

    private String b;

    public String getA() {
        return a;
    }

    public String getB_in_A() {
        return b;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("A");
        sb.append("{a='").append(a).append('\'');
        sb.append(", b='").append(b).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
