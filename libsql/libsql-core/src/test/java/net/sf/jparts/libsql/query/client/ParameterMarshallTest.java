package net.sf.jparts.libsql.query.client;

import org.junit.Assert;
import net.sf.jparts.libsql.AbstractMarshallTest;
import org.junit.Test;

public class ParameterMarshallTest extends AbstractMarshallTest {

    private final static String pXml =
            "<qc:parameter xmlns:qc=\"http://jparts.sf.net/libsql/query/client\">" +
            "<qc:name>name</qc:name><qc:value>123</qc:value></qc:parameter>";
//            "<parameter>" +
//            "<name>name</name>" +
//            "<value>123</value>" +
//            "</parameter>";

    @Test
    public void testMarshall() {
        Parameter p = new Parameter("name", "123");
        Assert.assertEquals(pXml, marshall(p));
    }
}
