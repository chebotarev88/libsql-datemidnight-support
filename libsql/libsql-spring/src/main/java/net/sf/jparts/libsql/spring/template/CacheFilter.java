package net.sf.jparts.libsql.spring.template;

/**
 * Is used in {@link AdvancedNamedParameterJdbcTemplate} to determine whether it is possible to cache query.
 *
 * <p>Implementations must be thread-safe.
 */
public interface CacheFilter {

    /**
     * Determine whether it is possible to cache query.
     *
     * @param sql not {@code null}.
     * @return {@code true} if query can be cached, {@code false} otherwise.
     *
     * @throws CacheFilterException in case of an error occurred.
     */
    public boolean satisfied(String sql) throws CacheFilterException;
}
