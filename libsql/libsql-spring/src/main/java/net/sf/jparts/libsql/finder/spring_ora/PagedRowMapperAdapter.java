package net.sf.jparts.libsql.finder.spring_ora;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

/**
 * Adapts any {@link RowMapper} to use with {@link OracleSpringJdbcFinder}.
 */
public class PagedRowMapperAdapter<T> extends AbstractPagedRowMapper<T> {

    private RowMapper<T> delegate;

    public PagedRowMapperAdapter(RowMapper<T> delegate) {
        if (delegate == null) {
            throw new IllegalArgumentException("delegate mapper must not be null");
        }
        this.delegate = delegate;
    }

    @Override
    public T mapRowInternal(ResultSet rs, int rowNum) throws SQLException {
        return delegate.mapRow(rs, rowNum);
    }
}
