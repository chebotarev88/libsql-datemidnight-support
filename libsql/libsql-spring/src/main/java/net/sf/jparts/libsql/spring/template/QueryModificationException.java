package net.sf.jparts.libsql.spring.template;

import org.springframework.dao.DataAccessException;

public class QueryModificationException extends DataAccessException {

    public QueryModificationException(String msg) {
        super(msg);
    }

    public QueryModificationException(String msg, Throwable cause) {
        super(msg, cause);
    }
}
